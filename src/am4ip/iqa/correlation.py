
import torch
from abc import ABC, abstractmethod


class Correlation(ABC):
    """Abstract class to compute random variable correlation coefficient.
    """
    def __init__(self, name: str) -> None:
        self.name = name

    @abstractmethod
    def __call__(self, x: torch.Tensor, y: torch.Tensor) -> float:
        """ Compute correlation coefficient between random variables X and Y.

        :param x: Set of realisations for random variable X. Size = N
        :param y: Set of realisations for random variable Y. Size = N
        :return: Correlation coefficient.
        """
        raise NotImplementedError


class Pearson(Correlation):
    """Abstract class to compute random variable correlation coefficient.
    """
    def __init__(self) -> None:
        super(Pearson, self).__init__(name='Pearson Correlation Coefficient')

    def __call__(self, x: torch.Tensor, y: torch.Tensor) -> float:
        mean_x = torch.mean(x)
        mean_y = torch.mean(y)

        cov_xy = torch.mean((x - mean_x) * (y - mean_y))
        std_x = torch.sqrt(torch.mean((x - mean_x) ** 2))
        std_y = torch.sqrt(torch.mean((y - mean_y) ** 2))

        return cov_xy / (std_x * std_y)


class SpearmanV1(Pearson):
    """Abstract class to compute random variable correlation coefficient.
    """
    def __init__(self) -> None:
        super(SpearmanV1, self).__init__()
        self.name = "Spearman Rank-Correlation Coefficient (v1)"

    def __call__(self, x: torch.Tensor, y: torch.Tensor) -> float:
        order_x = torch.argsort(x)
        rank_x = torch.empty(order_x.size(0), device=order_x.device, dtype=torch.float32)
        rank_x[order_x] = torch.arange(order_x.size(0), device=order_x.device, dtype=torch.float32)

        order_y = torch.argsort(y)
        rank_y = torch.empty(order_y.size(0), device=order_y.device, dtype=torch.float32)
        rank_y[order_y] = torch.arange(order_y.size(0), device=order_y.device, dtype=torch.float32)

        return super(SpearmanV1, self).__call__(rank_x, rank_y)


class SpearmanV2(Correlation):
    """Abstract class to compute random variable correlation coefficient.
    """
    def __init__(self) -> None:
        super(SpearmanV2, self).__init__(name="Spearman Rank-Correlation Coefficient (v2)")

    def __call__(self, x: torch.Tensor, y: torch.Tensor) -> float:
        order_x = torch.argsort(x)
        rank_x = torch.empty(order_x.size(0), device=order_x.device, dtype=torch.float32)
        rank_x[order_x] = torch.arange(order_x.size(0), device=order_x.device, dtype=torch.float32)

        order_y = torch.argsort(y)
        rank_y = torch.empty(order_y.size(0), device=order_y.device, dtype=torch.float32)
        rank_y[order_y] = torch.arange(order_y.size(0), device=order_y.device, dtype=torch.float32)

        srcc = 1 - 6 * torch.sum((rank_x - rank_y) ** 2) / (x.size(0) * (x.size(0) ** 2 - 1))

        return srcc
