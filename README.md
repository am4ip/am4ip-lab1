# Advanced Methods for Image Processing - Lab 1

## Introduction
This is the code repository for the first lab on Image Quality Assessment. It requires a clear understanding
of courses 1 and 2. While the first part will be mostly focused on understanding the given API (dataset handling,
IQ metric implementation, correlation with MOS, ...), the second part will be about extending it by
implementing other IQ metrics.

Gitlab pages are automatically generated from this repository, and include:
- The lab instructions
- A documentation of the API (generated from the docstrings)

Refer to [this page](https://gitlab.com/am4ip/am4ip-lab1) to access to the lab instructions. To configure
your environment, please refer to the [Installation](#installation) section.


## Installation
An environment with a pre-configured Python + Torch installation using GPUs is available. Please follow
[this link.](https://dept-info.labri.fr/~mansenca/public/CREMI_deeplearning_env.pdf)

Then, you have to add the [src](src) folder to your python path, so it can find the given API. In Linux
it is done by running the following command, after replacing with the correct path (e.g., using pwd command
in src folder): 
```bash
export PYTHONPATH = $PYTHONPATH:/absolute/path/to/src
```
Note that this should be done everytime if you are running scripts in a terminal.

In a Jupyter Notebook (and even in a script), it can be done manually as follows:
```python
import sys
sys.path.append("/absolute/path/to/src")
```
before doing any import of the am4ip library.

One can also configure its IDE for the project.

### In Pycharm:
- right Click on the src folder
- Mark Directory as > Sources Root


## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/am4ip/am4ip-lab1/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing (SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
