.. am4ip

Advanced Methods for Image Processing - Lab 1
=============================================

.. toctree::
   :maxdepth: 2
   :caption: Image Quality Assessment

   exercises
   api
